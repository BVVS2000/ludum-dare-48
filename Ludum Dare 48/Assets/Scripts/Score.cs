﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{
    [SerializeField] private float scoreInreseFactor = 1f;

    [SerializeField] private GameObject canvas;
    [SerializeField] private GameObject highScoreText;
    [SerializeField] private TMPro.TextMeshProUGUI scoreText;

    private float _score = 0;
    private float _highScore = 0;
    public float amount
    {
        get
        {
            return _score;
        }
    }

    public float highScore
    {
        get
        {
            return _highScore;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == 4)
        {
            _score += scoreInreseFactor * Time.deltaTime;
        }
    }

    public void DeathScreen()
    {
        canvas.SetActive(true);
        if(_score > _highScore)
        {
            highScoreText.SetActive(true);
        }

        scoreText.text = "Score: " + (int)_score;
    }

    public void ResetGame()
    {
        Tile.ResetCount();
        canvas.SetActive(false);
        highScoreText.SetActive(false);
        ResetScore();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ResetScore()
    {
        _highScore = _score;
        _score = 0;
    }
}
