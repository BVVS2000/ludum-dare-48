﻿using UnityEngine;
using System.Linq;

public class Tile : MonoBehaviour
{
    private static int _count = 0;
    public static int count
    {
        get
        {
            return _count;
        }
    }
    public int id;
    public float tileOffset;

    [SerializeField] private float speed = 0.0f;
    [SerializeField] private bool staticTile = false;
    [SerializeField] private float destroyAtPosY = 0.0f;

    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        if (!staticTile)
        {
            id = _count++;

            if (id != 0)
            {
                Move();
            }
        }
    }

    private void Update()
    {
        rb.velocity = Vector2.up * speed;
        if(transform.position.y>=destroyAtPosY)
        {
            Destroy(gameObject);
        }
    }

    public void Move()
    {
        float offset = FindObjectsOfType<Tile>().Where(t => t.id == id - 1).FirstOrDefault().transform.position.y;

        transform.position = new Vector2(0, offset - tileOffset);
    }

    public static void ResetCount()
    {
        _count = 0;
    }
}
