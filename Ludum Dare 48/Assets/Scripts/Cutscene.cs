﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cutscene : MonoBehaviour
{
    private int activeScene
    {
        get
        {
            return SceneManager.GetActiveScene().buildIndex;
        }
    }

    private void Update()
    {
        if(Input.anyKey)
        {
            NextScene();
        }
    }

    public void NextScene()
    {
        SceneManager.LoadScene(activeScene + 1);
    }
}

