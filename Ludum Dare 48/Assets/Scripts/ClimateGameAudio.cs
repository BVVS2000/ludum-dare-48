﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimateGameAudio : MonoBehaviour
{
    public AudioSource demon;
    public AudioSource armor_death;

    public List<AudioClip> armors;
    public List<AudioClip> demons;

    public AudioClip death;
    public void ArmorPlay()
    {
        int rand = Random.Range(0,4);
        armor_death.clip = armors[rand];
        armor_death.Play();
        if (!demon.isPlaying && Random.Range(0, 10)==5)
        {
            rand = Random.Range(0, 3);
            demon.clip = demons[rand];
            demon.Play();
        }
    }

    public void JumpCut()
    {
        armor_death.Stop();
    }

    public void DeathSound()
    {
        armor_death.clip = death;
        armor_death.Play();
    }
}
