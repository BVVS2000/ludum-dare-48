﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum Side { left, right }

public class PlayerMovement : MonoBehaviour
{
    //[SerializeField] private float fallOffSpeed = 0.0f;
    [SerializeField] private float jumpSpeed = 0.0f;
    [SerializeField] private Vector2 jumpVector = Vector2.zero;


    [SerializeField] private UnityEvent onJump;
    [SerializeField] private UnityEvent onStickToWall;
    [SerializeField] private LayerMask WallsLayerMask;
    [SerializeField] private LayerMask FloorLayerMask;

    private BoxCollider2D collider;
    private Rigidbody2D rb;

    private Side _stickedTo = Side.left;
    private bool _stickedToWall = true;

    public Side stickedTo
    {
        get
        {
            return _stickedTo;
        }
    }

    public bool stickedToWall
    {
        get
        {
            bool result = false;

            RaycastHit2D hitRight = Physics2D.BoxCast(collider.bounds.center, collider.bounds.size, 0f, Vector2.right, 0.05f, WallsLayerMask);
            RaycastHit2D hitLeft = Physics2D.BoxCast(collider.bounds.center, collider.bounds.size, 0f, Vector2.left, 0.05f, WallsLayerMask);

            if(hitRight)
            {
                _stickedTo = Side.right;
            }
            if(hitLeft)
            {
                _stickedTo = Side.left;
            }

            result = (hitRight.collider != null || hitLeft.collider != null);

            if (result && result != _stickedToWall)
            {
                onStickToWall.Invoke();
                rb.constraints = (RigidbodyConstraints2D)5; // RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX
                rb.velocity = Vector2.zero;
            }

            return result;
        }
    }

    private void Start()
    {
        collider = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        _stickedToWall = stickedToWall;
        if (Input.anyKeyDown && stickedToWall)
        {
            RaycastHit2D hitFloor = Physics2D.BoxCast(collider.bounds.center, collider.bounds.size, 0f, Vector2.down, 0.05f, FloorLayerMask);

            if (hitFloor.collider != null)
            {
                Jump();
            }
        }
    }

    private void Jump()
    {
        onJump.Invoke();
        rb.constraints = (RigidbodyConstraints2D) 4; // RigidbodyConstraints2D.FreezeRotation

        float direction = (stickedTo == Side.left) ? 1f : -1f;
        jumpVector.Normalize();
        rb.velocity = Vector2.zero;
        rb.AddForce( new Vector2(jumpVector.x * direction, jumpVector.y)  * jumpSpeed);
    }
}
