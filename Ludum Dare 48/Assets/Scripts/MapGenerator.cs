﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Xml.XPath;

public class MapGenerator : MonoBehaviour
{
    [SerializeField] private Vector2 spawnTilePosition;
    [Space]
    [SerializeField] private float tileOffset = 0.0f;
    [SerializeField] private float wallOffset = 0.0f;
    [SerializeField] private float obstacleOffsetX = 0.0f;
    [SerializeField] private float obstacleOffsetMinY = 0.0f;
    [SerializeField] private float obstacleOffsetMaxY = 0.0f;
    [SerializeField] private float obstacleOffsetBetween = 0.0f;
    [SerializeField] private float obstacleHeight = 0.0f;
    [SerializeField] private float backgroundBaseOffset = 0.0f;
    [SerializeField] private float backgroundOffset = 0.0f;
    [Space]
    [SerializeField] private int obstaclesPerTile = 0;
    [SerializeField] private float tileSpawnTimeOffset = 0.0f;
    [SerializeField] private int tilesWithoutObstacles = 5;
    [SerializeField] private float tileDespawnTime = 0.0f;
    [SerializeField] private float backgroundTiles = 0f;

    private float timmer = 0.0f;
    private int wave = 0;
    [Space]
    [SerializeField] private GameObject mapObject;
    [Space]
    [SerializeField] private Tile emptyTile;
    [SerializeField] private GameObject wallObject;
    [SerializeField] private GameObject dirtObject;
    [SerializeField] private List<GameObject> obstacles;

    private int PlacesOnWallForObstacles
    {
        get
        {
            return (int)Mathf.Abs((obstacleOffsetMaxY - obstacleOffsetMinY) / (obstacleHeight + obstacleOffsetBetween));
        }
    }

    private float PlaceObstacleOffset
    {
        get
        {
            return obstacleHeight + obstacleOffsetBetween;
        }
    }

    private void Start()
    {
        SpawnTile();
        Debug.Log(PlacesOnWallForObstacles);
        Debug.Log(PlaceObstacleOffset);
    }

    private void Update()
    {
        if (timmer > 0)
        {
            timmer -= Time.deltaTime;
        }

        if (timmer <= 0)
        {
            SpawnTile();
        }
    }

    private void SpawnTile()
    {
        timmer = tileSpawnTimeOffset;
        Transform tile = Instantiate(emptyTile, spawnTilePosition, Quaternion.identity, mapObject.transform).transform;
        Transform wallRight = Instantiate(wallObject, tile.position + new Vector3(wallOffset, 0, 0), Quaternion.identity, tile).transform;
        Transform wallLeft = Instantiate(wallObject, tile.position + new Vector3(-wallOffset, 0, 0), Quaternion.identity, tile).transform;

        for (int i = 0; i < backgroundTiles; i++)
        {
            Instantiate(dirtObject, tile.position + new Vector3(-backgroundBaseOffset - backgroundOffset * i, 0, 0), Quaternion.identity, tile);
            Instantiate(dirtObject, tile.position + new Vector3(backgroundBaseOffset + backgroundOffset * i, 0, 0), Quaternion.identity, tile);
        }

        if (wave++ >= tilesWithoutObstacles)
        {
            wave = 0;
            List<int> buffer = new List<int>();
            for (int i = 0; i < obstaclesPerTile; i++)
            {
                int place = -1;

                List<Obstacle> obstaclesInTile = FindObjectsOfType<Obstacle>().Where(t => t.transform.parent == wallLeft || t.transform.parent == wallRight).ToList();
                
                if (obstaclesInTile.Count > 0)
                {
                    do
                    {
                        int result = Random.Range(0, PlacesOnWallForObstacles + 1);
                        if(!buffer.Contains(result))
                        {
                            place = result;
                        }

                    } while (place == -1);
                }
                else
                {
                    place = Random.Range(0, PlacesOnWallForObstacles + 1);
                    buffer.Add(place);
                }

                if (Random.Range(0, 2) == 0)
                {
                    float y = obstacleOffsetMaxY;
                    if(place>0)
                    {
                        y = obstacleOffsetMaxY - PlaceObstacleOffset * place;
                    }
                    Instantiate(obstacles[Random.Range(0, obstacles.Count)], tile.position + new Vector3(-obstacleOffsetX, y, 0), Quaternion.identity, wallLeft);
                }
                else
                {
                    GameObject obstacle = Instantiate(obstacles[Random.Range(0, obstacles.Count)], tile.position + new Vector3(obstacleOffsetX, obstacleOffsetMaxY - PlaceObstacleOffset * place, 0), Quaternion.identity, wallRight);
                    obstacle.GetComponent<SpriteRenderer>().flipX = true;
                }
            }
        }

        tile.GetComponent<Tile>().tileOffset = tileOffset;
    }
}