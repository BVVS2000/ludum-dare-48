﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerSpriteController : MonoBehaviour
{
    [SerializeField] private Sprite neutralPos;
    private SpriteRenderer sprite;
    private Sprite temp;
    private void Start()
    {
        sprite = transform.Find("sprite").GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if(transform.position.x >= -1 && transform.position.x <= 1)
        {
            if(sprite.sprite != neutralPos)
            {
                temp = sprite.sprite;
                sprite.sprite = neutralPos;
            }
        }
        else if(sprite.sprite == neutralPos)
        {
            sprite.sprite = temp;
        }

        if(transform.position.x > 0)
        {
            sprite.flipX = false;
        }
        else
        {
            sprite.flipX = true;
        }
    }

    public void Flip()
    {
        sprite.flipX = !sprite.flipX;
    }

    public void Death()
    {
        sprite.color = Color.red;
    }
}
