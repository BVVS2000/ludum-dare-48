﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BackgroundMovement : MonoBehaviour
{
    [SerializeField] private float endPosY = 0f;
    [SerializeField] private float speed = 0f;

    public int id;

    private void Update()
    {
        if(transform.position.y >= endPosY)
        {
            if(id==0)
            {
                transform.position = new Vector2(0, FindObjectsOfType<BackgroundMovement>().Where(t => t.id == FindObjectsOfType<BackgroundMovement>().Length - 1).FirstOrDefault().transform.position.y - 2.25f);
            }
            else
            {
                transform.position = new Vector2(0, FindObjectsOfType<BackgroundMovement>().Where(t => t.id == id-1).FirstOrDefault().transform.position.y - 2.25f);
            }
        }

        transform.position = Vector2.MoveTowards(transform.position, (Vector2)transform.position + Vector2.up, speed * Time.deltaTime);
    }
}
