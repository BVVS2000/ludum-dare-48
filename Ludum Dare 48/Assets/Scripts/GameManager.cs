﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private UnityEvent onDeath;
    private GameObject player;
    private Score score;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        score = FindObjectOfType<Score>();
    }

    public void Death()
    {
        onDeath.Invoke();
        score.DeathScreen();
    }
}
