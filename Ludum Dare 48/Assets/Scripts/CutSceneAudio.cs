﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CutSceneAudio : MonoBehaviour
{
    public AudioSource Kamera;
    public TMPro.TextMeshProUGUI gopnik;
    
    
    // Update is called once per frame
    public void Play(AudioClip sound)
    {
        Kamera.clip = sound;
        Kamera.Play();
            
    }

    public void Say(string text)
    {
        gopnik.text = text;
    }
}
